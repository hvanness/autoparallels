webpackHotUpdate(0,{

/***/ 305:
/***/ function(module, exports, __webpack_require__) {

	eval("'use strict';\n\nObject.defineProperty(exports, \"__esModule\", {\n  value: true\n});\n\nvar _gammas = __webpack_require__(303);\n\nvar _array = __webpack_require__(307);\n\nexports.default = function (pos, vel) {\n  var coords = (0, _array.indices)(pos);\n  return coords.map(function (i) {\n    return coords.reduce(function (sum, j) {\n      return sum - coords.reduce(function (sum, k) {\n        return sum + _gammas.gammaThetaPhi[i][j][k](pos) * vel[j] * vel[k];\n      }, 0);\n    }, 0);\n  });\n};//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiMzA1LmpzIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vL3NyYy91dGlsL2NhbGNBY2NlbC5qcz9mNWY1Iiwid2VicGFjazovLy8/ZDQxZCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBnYW1tYVRoZXRhUGhpIH0gZnJvbSAnLi4vZ2FtbWFzJ1xuaW1wb3J0IHsgaW5kaWNlcyB9IGZyb20gJy4vYXJyYXknXG5cbmV4cG9ydCBkZWZhdWx0IChwb3MsIHZlbCkgPT4ge1xuICBjb25zdCBjb29yZHMgPSBpbmRpY2VzKHBvcylcbiAgcmV0dXJuIGNvb3Jkcy5tYXAoaSA9PiB7XG4gICAgcmV0dXJuIGNvb3Jkcy5yZWR1Y2UoKHN1bSwgaikgPT4ge1xuICAgICAgcmV0dXJuIHN1bSAtIGNvb3Jkcy5yZWR1Y2UoKHN1bSwgaykgPT4ge1xuICAgICAgICByZXR1cm4gc3VtICsgZ2FtbWFUaGV0YVBoaVtpXVtqXVtrXShwb3MpICogdmVsW2pdICogdmVsW2tdXG4gICAgICB9LCAwKVxuICAgIH0sIDApXG4gIH0pXG59XG5cblxuXG4vKiogV0VCUEFDSyBGT09URVIgKipcbiAqKiBzcmMvdXRpbC9jYWxjQWNjZWwuanNcbiAqKi8iLCJ1bmRlZmluZWRcblxuXG4vKiogV0VCUEFDSyBGT09URVIgKipcbiAqKiBcbiAqKi8iXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUNDQTtBRENBO0FDQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOyIsInNvdXJjZVJvb3QiOiIifQ==");

/***/ }

})