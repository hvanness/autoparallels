var webpack = require('webpack')
var path = require('path')

module.exports = {
  entry: [
    'babel-polyfill',
    './src/index.js'
  ],

  output: {
    path: path.join(__dirname, 'public/static'),
    filename: 'bundle.js',
    contentBase: path.join(__dirname, 'public'),
    publicPath: '/static/'
  },

  devtool: 'cheap-module-eval-source-map',

  module: {
    preLoaders: [
      {
        test: /\.js$/,
        loaders: ['eslint'],
      }
    ],
    loaders: [
      {
        test: /\.js?$/,
        loaders: ['babel'],
        include: path.join(__dirname, 'src')
      },
    ]
  },

  resolve: {
    extensions: ['', '.js'],
    modulesDirectories: ['node_modules'],
    root: path.resolve('./src')
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
  ]
}

