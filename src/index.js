import * as THREE from 'three'
import Skydome from './Skydome'
import Planets from './Planets'

const OrbitControls = require('three-orbit-controls')(THREE)

var render = function () {
  requestAnimationFrame(render)

  planets.update()

  skydome.camera.quaternion.copy(camera.quaternion)
  skydome.camera.updateMatrix()

  renderer.render(skydome.scene, skydome.camera)
  renderer.render(scene, camera)
}

var scene = new THREE.Scene()
var camera = new THREE.PerspectiveCamera(45, window.innerWidth/window.innerHeight, 0.1, 1000)
camera.position.z = 100

var renderer = new THREE.WebGLRenderer({
  preserveDrawingBuffer: true,
  alpha: true,
  antialias: true
})
renderer.setSize( window.innerWidth, window.innerHeight )
renderer.autoClearColor = false
renderer.setClearColor(new THREE.Color(0x000000))
renderer.clear()
document.body.appendChild(renderer.domElement)

var skydome = new Skydome()
new OrbitControls(camera, renderer.domElement)
var planets = new Planets(8)
scene.add(planets.mesh)
render()
