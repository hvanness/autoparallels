import * as THREE from 'three'
import { cap, add } from './util/vector'
import calcAccel from './util/calcAccel'


const to3Sphere = (r, v) => [r, v[0], v[1]]

const sphericalChartInverse = (v) => new THREE.Vector3(
  v[0] * Math.sin(v[1]) * Math.cos(v[2]),
  v[0] * Math.sin(v[1]) * Math.sin(v[2]),
  v[0] * Math.cos(v[1])
)

const toCartesian = x => sphericalChartInverse(to3Sphere(8,x))

var Planet = function () {
  let a = [0,0]
  let x = [
    Math.PI/2,
    2*Math.PI*Math.random(),
  ]
  let v = [
    0.01*Math.random(),
    0.05
  ]

  this.pos = toCartesian(x)

  this.update = function () {
    v = cap(0.4, add(v, a))
    a = calcAccel(x, v)
    x = add(x, v)

    this.pos = toCartesian(x)
  }
}

export default Planet
