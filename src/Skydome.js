import * as THREE from 'three'

var Skydome = function () {
    this.scene = new THREE.Scene();
    this.camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 1, 20000);

    var uniforms = {
        texture: { type: 't', value: THREE.ImageUtils.loadTexture('/res/eso0932a.jpg') }
    };

    var material = new THREE.ShaderMaterial( {
          uniforms:       uniforms,
          vertexShader:   document.getElementById('sky-vertex').textContent,
          fragmentShader: document.getElementById('sky-fragment').textContent
    });
    material.side = THREE.DoubleSide;
    this.mesh = new THREE.Mesh(new THREE.SphereGeometry(1000, 60, 40), material);
    this.mesh.scale.set(-1, 1, 1);
    this.mesh.rotation.order = 'XZY';
    this.mesh.renderDepth = 1000.0;

    this.scene.add(this.mesh);
}

export default Skydome
