export const sMultiply = (s, v) => v.map(v_i => s * v_i)
export const norm = v => Math.sqrt(v.reduce((sum, v_i) => sum + v_i * v_i, 0))
export const normalise = v => norm(v) == 0 ? v : sMultiply(1/norm(v), v)

export const cap = (l, v) => {
  const v1 = sMultiply(l, normalise(v))
  return norm(v) < norm(v1) ? v : v1
}

export const add = (v, w) => v.map((x, i) => v[i] + w[i])
