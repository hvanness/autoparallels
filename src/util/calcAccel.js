import { gammaThetaPhi } from '../gammas'
import { indices } from './array'

export default (pos, vel) => {
  const coords = indices(pos)
  return coords.map(i => {
    return coords.reduce((sum, j) => {
      return sum - coords.reduce((sum, k) => {
        return sum + gammaThetaPhi[i][j][k](pos) * vel[j] * vel[k]
      }, 0)
    }, 0)
  })
}
