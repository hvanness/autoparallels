const zero = () => 0
const sin = Math.sin
const cos = Math.cos
const cot = x => x % Math.PI != 0 ? cos(x)/sin(x) : 0

export const gammaThetaPhi = [
  [
    [zero,                        zero],
    [zero, p => -sin(p[0]) * cos(p[0])]
  ],
  [
    [ zero,           p => cot(p[0])],
    [ p => cot(p[0]),           zero],
  ],
]

export const gamma3 = [
  [
    [zero,          zero,               zero],
    [zero, p => -sin(p[0]) * cos(p[0]), zero],
    [zero,          zero,               zero],
  ],
  [
    [ zero,           p => cot(p[0]), zero],
    [ p => cot(p[0]),      zero,      zero],
    [ zero,                zero,      zero],
  ],
  [
    [ zero,           p => cot(p[0]), zero],
    [ p => cot(p[0]),      zero,      zero],
    [ zero,                zero,      zero],
  ],
]
