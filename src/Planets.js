import * as THREE from 'three'
import Planet from './Planet'

var Planets = function (numPlanets) {
    this.planets = [];

    var texture = THREE.ImageUtils.loadTexture("res/circle.png");
    var material = new THREE.PointCloudMaterial({sizeAttenuation: false, size: 8, map: texture, transparent : true, alphaTest: 0.1});
    var geometry = new THREE.Geometry();
    geometry.dynamic = true;


    this.update = function () {
        for (var i=0; i<numPlanets; i++) {
            this.planets[i].update();
            geometry.vertices[i].copy(this.planets[i].pos);
            geometry.verticesNeedUpdate = true;
        }
    }

    for (var i=0; i<numPlanets; i++) {
        this.planets[i] = new Planet();
        geometry.vertices.push(this.planets[i].pos);
    }

    this.mesh = new THREE.PointCloud(geometry, material);
}

export default Planets
